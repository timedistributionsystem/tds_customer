package com.example.tng0715.timedistributionsystemcustomer;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            FragmentDeliveryStatus fragmentDeliveryStatus = new FragmentDeliveryStatus();
            return fragmentDeliveryStatus;
        }
        else if(position == 1){
            FragmentStorefront fragmentStorefront = new FragmentStorefront();
            return fragmentStorefront;
        }
        else if(position == 2)             // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            FragmentRenewal fragmentRenewal = new FragmentRenewal();
            return fragmentRenewal;
        }
        else if(position == 3)
        {
            FragmentTep fragmentTep = new FragmentTep();
            return fragmentTep;
        }
        else if(position == 4)
        {
            FragmentCsSite fragmentCsSite = new FragmentCsSite();
            return fragmentCsSite;
        }
        else
        {
            FragmentSpecial fragmentSpecial = new FragmentSpecial();
            return fragmentSpecial;
        }



    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}