package com.example.tng0715.timedistributionsystemcustomer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.http.util.EncodingUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCsSite extends Fragment {
    private WebView wvCsSite;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cssite, container, false);
        String url = "https://www.timeasiasubs.com/service.php";

        wvCsSite = (WebView) v.findViewById(R.id.wvCsSite);
        WebSettings webSettings = wvCsSite.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wvCsSite.setWebViewClient(new WebViewClient() {
        });

        wvCsSite.postUrl(url, EncodingUtils.getBytes("", "BASE24"));
        return v;
    }


}
