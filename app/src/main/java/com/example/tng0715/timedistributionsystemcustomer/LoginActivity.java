package com.example.tng0715.timedistributionsystemcustomer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class LoginActivity extends Activity {

    Context c;

    EditText etUserID;
    EditText etEmail;
    String userID;
    String email;

    boolean checkLogin;

    ImageButton ibLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("LoginActivity Born!");

        c = this;

        setContentView(R.layout.activity_login);

        etUserID = (EditText) findViewById(R.id.etUserID);
        etEmail = (EditText) findViewById(R.id.etEmail);
        checkLogin = false;

        etUserID.setText("991506176");
        etEmail.setText("test150616k@test150616k.com");

        ibLogin = (ImageButton) findViewById(R.id.ibLogin);

        ibLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Login button hit!");

                userID = etUserID.getText() + "";
                email = etEmail.getText() + "";
                _("userID:" + userID);
                _("email:" + email);

                if (userID.length() == 0 || email.length() == 0) {
                    Toast.makeText(c, "Please fill in userID and email", Toast.LENGTH_SHORT).show();
                    return;
                }

                task t = new task();
                t.execute();
            }
        });


    }

    class task extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
            String url_select = "http://www.timeasiasubs.com/timeasia/custCheckLogin.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userNo", userID));
            param.add(new BasicNameValuePair("userEmail", email));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                _("Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
                _("Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");
                JSONObject output = json.getJSONObject("output");
                userID = output.getString("cust_no");
                email = output.getString("cust_mail");
                String name = output.getString("cust_name");
                String keyline = output.getString("cust_keyline");
                String cou = keyline.substring(0,Math.min(keyline.length(),3));

                _("Json value :" + server_status);
                if (server_status) {
                    _("Login Successfully");
// Toast.makeText(c, "Login Successfully", Toast.LENGTH_SHORT).show();
                    if (!checkLogin) {
                        Intent it = new Intent();
                        it.setClass(LoginActivity.this, HomeActivity.class);
                        it.putExtra("userID", userID);
                        it.putExtra("userEmail",email);
                        it.putExtra("userName",name);
                        it.putExtra("userCou",cou);
                        startActivity(it);
                        checkLogin = true;
                    }
                    finish();
                } else {
                    //text_1.append(id+"\t\t"+name+"\t\t"+email+"\t\t"+"\n");
                    _("Login Fail");
                    Toast.makeText(c, "Please Check Your UserID and Password", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                // TODO: handle exception
                _("Error parsing data " + e.toString());
            }
        }
    }

    private void _(String s) {
        Log.d("LoginTesting", "LoginActivity" + "########" + s);
    }
}
