package com.example.tng0715.timedistributionsystemcustomer;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class FragmentDeliveryStatus extends Fragment {

    ListView listView;
    String bundleID;
    Button btReport;

    int join = R.drawable.ic_join;
    int empty = R.drawable.ic_empty;
    int bundle = R.drawable.ic_box;
    int error = R.drawable.ic_error;
    int info = R.drawable.ic_information;

    Boolean canReport;
    Boolean hasDeliver;

    DeliveryStatusAdapter adapter;
    Calendar calendar;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_delivery_status, container, false);
        canReport = false;
        hasDeliver = false;
        listView = (ListView) v.findViewById(R.id.listView);
        btReport = (Button) v.findViewById(R.id.btReport);
        btReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (canReport) {
                    /*FragmentManager FM = getFragmentManager();
                    FragmentTransaction FT = FM.beginTransaction();
                    FragmentReport FQR = new FragmentReport();

                    FT.replace(R.id.pager, FQR);
                    FT.commit();*/

                    HomeActivity hmActivity = (HomeActivity) getActivity();
                    String userID = hmActivity.getUserID();
                    String email = hmActivity.getUserEmail();
                    Intent it = new Intent();
                    it.setClass(getActivity(), ReportActivity.class);
                    it.putExtra("userID", userID);
                    it.putExtra("userEmail",email);
                    startActivity(it);
                } else if (hasDeliver) {
                    Toast.makeText(getActivity(), "You product has been delivered to your address. ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "You product is being delivered to your address. Please be patient. ", Toast.LENGTH_LONG).show();
                }
            }
        });

        adapter = new DeliveryStatusAdapter(v.getContext(), R.layout.row_delivery_status_layout);
        listView.setAdapter(adapter);
        adapter.clear();
        adapter.notifyDataSetChanged();

        calendar = Calendar.getInstance();

        new task().execute();

        return v;
    }

    private class task extends AsyncTask<String, String, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(getActivity());
        InputStream is = null;
        String result = "";

        protected void onPreExecute() {
            progressDialog.setMessage("Fetching data...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    task.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
            String url_select = "http://www.timeasiasubs.com/timeasia/custDeliveryStatus.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity hmActivity = (HomeActivity) getActivity();
            String userNo = hmActivity.getUserID();
            String userEmail = hmActivity.getUserEmail();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userNo", userNo));
            param.add(new BasicNameValuePair("userEmail", userEmail));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                Log.e("LoginTesting", "Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");
                boolean user_login = (boolean) json.get("user_login");
                boolean dist_report_status = (boolean) json.get("dist_report_status");
                String user_no = json.getString("user_no");

                if (user_login) {
                    Date delivery_date = null;

                    try {
                        JSONObject label_information = json.getJSONObject("label_information");
                        String lbl_bundle_no = label_information.getString("lbl_bundle_no");
                        String lbl_mag = label_information.getString("lbl_mag");
                        String lbl_type = label_information.getString("lbl_type");
                        String lbl_postalcode = label_information.getString("lbl_postalcode");
                        String lbl_cou_code = label_information.getString("lbl_cou_code");
                        DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join,
                                "Label Detail: " + lbl_bundle_no,
                                "Country Code:" + lbl_cou_code + "\nPostal Code:" + lbl_postalcode,
                                info);
                        adapter.add(dataProvider);
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "This Account Does Not Have Any Bundle!", Toast.LENGTH_LONG).show();
                    }
                    try {
                        JSONArray distribution_map = json.getJSONArray("distribution_map");
                        //Search The Array And Put Them Into The Adapter
                        int j = 0;
                        for (int i = 0; i < distribution_map.length(); i++) {
                            JSONObject distribution_map_object = distribution_map.getJSONObject(i);

                            //Get The distribution_map Array Data
                            String dm_checkpoint = distribution_map_object.getString("dm_checkpoint");
                            String dm_label_type = distribution_map_object.getString("dm_label_type");
                            String dm_mag = distribution_map_object.getString("dm_mag");
                            String dm_cou_code = distribution_map_object.getString("dm_cou_code");
                            String dm_postalcode = distribution_map_object.getString("dm_postalcode");
                            String dm_delivery_method = distribution_map_object.getString("dm_delivery_method");
                            String dm_checkpoint_desc = distribution_map_object.getString("dm_checkpoint_desc");
                            String dm_checkpoint_dt = distribution_map_object.getString("dm_checkpoint_dt");

                            delivery_date = dateFormat.parse(dm_checkpoint_dt);

                            try {
                                JSONArray delivery_status = json.getJSONArray("delivery_status");
                                JSONObject delivery_status_object = delivery_status.getJSONObject(i);

                                //Get The delivery_status Array Data
                                String ds_bundle_id = delivery_status_object.getString("ds_bundle_id");
                                int ds_did = delivery_status_object.getInt("ds_did");
                                String ds_postalcode = delivery_status_object.getString("ds_postalcode");
                                String ds_traffic_code = delivery_status_object.getString("ds_traffic_code");
                                String ds_cou_code = delivery_status_object.getString("ds_cou_code");
                                String ds_product_code = delivery_status_object.getString("ds_product_code");
                                String ds_delivery_method = delivery_status_object.getString("ds_delivery_method");
                                String ds_timestamp = delivery_status_object.getString("ds_timestamp");

                                if (distribution_map.length() == delivery_status.length()) {
                                    hasDeliver = true;
                                }

                                try {
                                    JSONObject delivery_status_object_next = delivery_status.getJSONObject(i + 1);

                                    DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, "Arrived at " + ds_timestamp, "Delivered to " + dm_checkpoint_desc, empty);
                                    adapter.add(dataProvider);
                                } catch (Exception e) {
                                    DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, "Arrived at " + ds_timestamp, "Delivered to " + dm_checkpoint_desc, bundle);
                                    adapter.add(dataProvider);
                                }

                                if (dist_report_status) {
                                    JSONArray dist_report = json.getJSONArray("dist_report");
                                    int dr_did;
                                    try {
                                        JSONObject dist_report_object = dist_report.getJSONObject(j);
                                        dr_did = dist_report_object.getInt("dr_did");
                                    } catch (Exception ex) {
                                        dr_did = -1;
                                    }
                                    while (ds_did == dr_did) {
                                        try {
                                            JSONObject dist_report_object = dist_report.getJSONObject(j);
                                            dr_did = dist_report_object.getInt("dr_did");
                                            String dr_bundle_id = dist_report_object.getString("dr_bundle_id");
                                            String dr_label_id = dist_report_object.getString("dr_label_id");
                                            String dr_message = dist_report_object.getString("dr_message");
                                            String dr_timestamp = dist_report_object.getString("dr_timestamp");
                                            DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join,
                                                    dr_timestamp,
                                                    "Report : " + dr_message,
                                                    error);
                                            adapter.add(dataProvider);

                                            j++;
                                            dist_report_object = dist_report.getJSONObject(j);
                                            dr_did = dist_report_object.getInt("dr_did");
                                        } catch (Exception ex) {
                                            break;
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, "Exp Date: " + dm_checkpoint_dt, "Delivering to " + dm_checkpoint_desc, empty);
                                adapter.add(dataProvider);
                            }

                        }

                        if (calendar.getTime().after(delivery_date) && !hasDeliver) {
                            canReport = true;
                        }
                    } catch (Exception ex) {
                        Log.d("TEST ############### :", ex.toString());
                        Toast.makeText(getActivity(), "This Bundle Does Not Have Any Distribution Map!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Unable To Connect To The Database", Toast.LENGTH_LONG).show();
                }

//                Toast.makeText(getActivity(), "Record(s) found.", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }

            this.progressDialog.dismiss();
        }
    }
}
