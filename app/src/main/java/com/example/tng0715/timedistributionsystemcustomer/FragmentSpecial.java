package com.example.tng0715.timedistributionsystemcustomer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.http.util.EncodingUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSpecial extends Fragment {
    private WebView wvSpecial;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_special, container, false);
        String url = "https://www.timeasiasubs.com/BATMANvSUPERMAN2016";

        wvSpecial = (WebView) v.findViewById(R.id.wvSpecial);
        WebSettings webSettings = wvSpecial.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wvSpecial.setWebViewClient(new WebViewClient() {
        });

        wvSpecial.postUrl(url, EncodingUtils.getBytes("", "BASE24"));
        return v;
    }


}
