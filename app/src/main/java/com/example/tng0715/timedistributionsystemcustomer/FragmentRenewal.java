package com.example.tng0715.timedistributionsystemcustomer;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class FragmentRenewal extends Fragment {
    private WebView wvRenewal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_renewal, container, false);
        String url = "https://www.timeasiasubs.com/renewal_order.php";
        HomeActivity hmActivity = (HomeActivity) getActivity();
        String country = hmActivity.getUserCou();
        String custNo = hmActivity.getUserID();
        String mail = hmActivity.getUserEmail();

        url = url + "?country=" + country + "&promoCode=&custNo=" + custNo + "&mail=" + mail;

        wvRenewal = (WebView) v.findViewById(R.id.wvRenewal);
        WebSettings webSettings = wvRenewal.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wvRenewal.setWebViewClient(new WebViewClient());

        wvRenewal.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.endsWith("new_landing.php") || url.endsWith("donor_landing.php") ||
                        url.endsWith("https://subscription.timeinc.com/storefront/privacy/time/generic_privacy_new.html?dnp-source=I") ||
                        url.endsWith("service.php") || url.endsWith("http://www.timeinc.net/subs/privacy/cookie.html") ||
                        url.endsWith("https://subscription.timeinc.com/storefront/privacy/time/privacy_terms_service.html") ||
                        url.endsWith("http://www.time.com/asiamail") || url.endsWith("http://www.time.com/upgrade/asia")) {

                } else {
                    // Load all other urls normally.
                    view.loadUrl(url);
                }
                return true;
            }
        });

        wvRenewal.loadUrl(url);

        return v;
    }


}
