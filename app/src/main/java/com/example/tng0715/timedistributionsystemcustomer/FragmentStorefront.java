package com.example.tng0715.timedistributionsystemcustomer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.http.util.EncodingUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentStorefront extends Fragment {
    private WebView wvStorefront;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_storefront, container, false);
        String url = "https://www.timeasiasubs.com/storefront/";

        wvStorefront = (WebView) v.findViewById(R.id.wvStorefront);
        WebSettings webSettings = wvStorefront.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wvStorefront.setWebViewClient(new WebViewClient() {
        });

        wvStorefront.postUrl(url, EncodingUtils.getBytes("", "BASE24"));
        return v;
    }


}
