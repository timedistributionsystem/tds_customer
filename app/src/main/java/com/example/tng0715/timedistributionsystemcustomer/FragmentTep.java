package com.example.tng0715.timedistributionsystemcustomer;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.http.util.EncodingUtils;

public class FragmentTep extends Fragment {

    private WebView wvTep;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tep, container, false);
        String url = "https://www.timeasiasubs.com/document.php";
        final String postData = "tid=TIS15TX504JQ4";
        final String tid = "TIS15TX504JQ4";
        final String js = "javascript: document.getElementsByName(\"tid\")[0].value = '" + tid + "';";

        wvTep = (WebView) v.findViewById(R.id.wvTep);
        WebSettings webSettings = wvTep.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wvTep.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.endsWith(".pdf")) {
                    //open the pdf with adobe acrobat
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    return true;
                } else if (url.endsWith("logout=1")) {

                } else {
                    // Load all other urls normally.
                    view.loadUrl(url);
                }

                return true;
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(js, new ValueCallback<String>() {

                        @Override
                        public void onReceiveValue(String value) {

                        }
                    });
                } else {
                    view.loadUrl(js);
                }
            }
        });

        //Post Method
        wvTep.postUrl(url, EncodingUtils.getBytes(postData, "BASE24"));

        return v;
    }

}
