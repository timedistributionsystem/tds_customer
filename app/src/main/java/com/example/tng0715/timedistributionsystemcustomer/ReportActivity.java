package com.example.tng0715.timedistributionsystemcustomer;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by hmng3 on 4/5/2016.
 */
public class ReportActivity extends Activity{
    private WebView wvReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        String url = "https://www.timeasiasubs.com/service.php";
        String custNo = getUserID();
        String mail = getUserEmail();

        Character[] custNoCharArray = toCharacterArray(custNo);

        String custNoJS = "";

        for (int i = 0; i < custNoCharArray.length-1; i++) {
            int j = i + 1;
            custNoJS += "document.getElementsByName(\"c" + j + "\")[0].value = '" + custNoCharArray[i] + "';";
        }

        final String js = "javascript: document.getElementsByName(\"mail\")[0].value = '" + mail + "';" + custNoJS;

        wvReport = (WebView) findViewById(R.id.wvReport);
        WebSettings webSettings = wvReport.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wvReport.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.endsWith("new_landing.php") || url.endsWith("renewal_landing.php") || url.endsWith("donor_landing.php") ||
                        url.endsWith("find_cust_num.php") || url.endsWith("contactus.php") ||
                        url.endsWith("http://www.time.com/time/?xid=site-tcm-td-wesctrl-AS1") ||
                        url.endsWith("https://subscription.timeinc.com/storefront/privacy/time/generic_privacy_new.html?dnp-source=I") ||
                        url.endsWith("http://www.timeinc.net/subs/privacy/cookie.html") ||
                        url.endsWith("http://cgi.timeinc.net/cgi-bin/mail/dnp/terms_of_service.cgi/time") ||
                        url.endsWith("javascript:bookmark();")) {
                } else {
                    // Load all other urls normally.
                    view.loadUrl(url);
                }

                return true;
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(js, new ValueCallback<String>() {

                        @Override
                        public void onReceiveValue(String value) {

                        }
                    });
                } else {
                    view.loadUrl(js);
                }
            }
        });

        wvReport.loadUrl(url);
    }

    public Character[] toCharacterArray(String s) {

        if (s == null) {
            return null;
        }

        int len = s.length();
        Character[] array = new Character[len];
        for (int i = 0; i < len; i++) {
            array[i] = new Character(s.charAt(i));
        }

        return array;
    }

    public String getUserID() {
        Bundle extra = getIntent().getExtras();
        String userID = "";
        if (extra != null) {
            userID = extra.getString("userID");
        }
        return userID;
    }

    public String getUserEmail() {
        Bundle extra = getIntent().getExtras();
        String userEmail = "";
        if (extra != null) {
            userEmail = extra.getString("userEmail");
        }
        return userEmail;
    }

}
