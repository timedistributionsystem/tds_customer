###Setup###

```
#!

git init
```

```
#!

git remote add origin <HTTPS>
```
###Pull###

```
#!

git branch --set-upstream-to=origin/master
```


```
#!

git pull origin master
```

###Push###
```
#!
git add .
```
```
#!
git commit -m "<comments>"
```
```
#!
git push origin master
```